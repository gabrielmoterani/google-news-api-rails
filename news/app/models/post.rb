class Post < ApplicationRecord
    def self.get_news(query)
        search = query || "Precatorio"
        browser = Watir::Browser.new :chrome , headless: true
        browser.goto("https://news.google.com/news/search/section/q/#{search}/#{search}")
        posts = browser.elements( :tag_name => "c-wiz", :class => "PaqQNc")
        data = []
        posts.each do |post|
            temp = {}
            temp[:url] = post.a.href
            temp[:title] = post.a.text
            if post.a.img.present?
                temp[:imgUrl] = post.a.img.src 
            end
            temp[:author] = post.spans.first.text
            temp[:date] = post.spans[1].text
            data << temp
        end
        browser.close
        data
    end
end
