module Crawlers
    class Crawler
        def self.getNews
            %x(rails db:drop db:create db:migrate)
            @search = "Precatorio"
            browser = Watir::Browser.start("https://news.google.com/news/search/section/q/#{@search}/#{@search}")
            posts = browser.elements( :tag_name => "c-wiz", class: "PaqQNc")
            data = []
            posts.each do |post|
                temp = {}
                temp[:url] = post.a.href
                temp[:title] = post.a.text
                if post.a.img.present?
                    temp[:imgUrl] = post.a.img.src 
                end
                temp[:author] = post.spans.first.text
                temp[:date] = post.spans[1].text
                Post.create! temp
            end
        end
    end
    Crawler.getNews.new
end

